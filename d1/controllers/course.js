const User = require('../models/User');
const Course = require('../models/Course');
const bcrypt = require('bcrypt');
const auth = require('../auth');

// module.exports.addCourse = (reqBody) => {
// 	let newCourse = new Course({
// 		name : reqBody.name,
// 		description: reqBody.description,
// 		price : reqBody.price
// 	});
// 	// Saves the created object to the database
// 	return newCourse.save().then((course, error) => {
// 		// Course creation failed
// 		if(error){
// 			return false;
// 		}
// 		// Course creation successful
// 		else {
// 			return true;
// 		}
// 	})
// }

// module.exports.addCourse = (reqBody) => {
// Course creation failed
// if(data.isAdmin){
// 		let newCourse = new Course({
// 		
// 		})

// 2. Refactor the addCourse controller method to implement admin authentication for creating a course.

// *Steps in setting admin access for course creation*:
// 1. Create a conditional statement that will check if the user is an administrator.

// 2. Create a new Course object using the mongoose model and the information from the request body and the id from the header.

// 3. Save the new Course to the database

module.exports.addCourse = (reqBody, userData) => {

    return Course.findById(userData.userId).then(result => {
        if (userData.isAdmin == false) {
            return "You're not an Admin"
        } else {
            let newCourse = new Course({
                name: reqBody.name,
                description: reqBody.description,
                price: reqBody.price
            })
        
            //Saves the created object to the database
            return newCourse.save().then((course, error) => {
                //Course creation failed
                if(error) {
                    return false
                } else {
                    //course creation successful
                    return "Course creation successful"
                }
            })
        }
    });
}

// CRUD
// Courses CRUD (via Code Log)
// Retrieve all courses
module.exports.getAllCourses = () => {
	return Course.find({}).then(result => {
		return result;
	})
}

// Retrieve all active courses
module.exports.getAllActive = () => {
	return Course.find({isActive : true}).then(result => {
		return result;
	})
};

// Retrieve specific course
module.exports.getCourse = (reqParams) => {
	return Course.findById(reqParams.courseId).then(result => {
		return result;
	})
}

// Update a course
module.exports.updateCourse = (reqParams, reqBody) => {
	let updatedCourse = {
		name : reqBody.name,
		description : reqBody.description,
		price : reqBody.price
	}
	return Course.findByIdAndUpdate(reqParams.CourseId, updatedCourse).then((course,error) => {
		if(error){
			return false;
		}else{
			return true;
		}
	})
}

// Updating Specific Course
module.exports.updateCourse = (reqParams, reqBody) => {
	let updatedCourse = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	}

	return Course.findByIdAndUpdate(reqParams.courseId, updatedCourse)
	.then((course, error) => {
		if (error) {
			return false;
		}else{
			return true;
		}
	})
}

// *Activity instructions:*
// 1. Create a route for archiving a course. The route must use JWT authentication and obtain the course ID from the url.

// 2. Create a controller method for archiving a course obtaining the course ID from the request params and the course information from the request body.
// 3. Process a PUT request at the /:courseId/archive route using postman to archive a course

module.exports.archiveCourse = (reqParams, reqBody) => {
	let Coursearchive ={
		isActive: false,
	};
	return Course.findByIdAndUpdate(reqParams.courseId, archiveCourse)
	.then((course, error) => {
		if (error) {
			return false;
		}else{
			return true;
		}

	})
}

// 4. Push to git with the commit message of Add activity code - S35.
// 5. Add the link in Boodle.

// Enroll user to a class
/* Steps:
1. Find the document in the database using the user's Id
2. Add the course Id to the user's enrollment array
3. Update the document in the MongoDB Atlas Database
 */
/*
 module.exports.enroll = async (data) => {
 	// Add the course ID in the enrollments array of the user
 	let isUserUpdated = await User.findById(data.userId).then(user => {
 		// Adds the courseId in the user's enrollment array
 		user.enrollments.push({courseId : data.courseId})
 		// Saves the updated user information in the database
 		return user.save().then((user, error) => {
 			if(error){
 				return false;
 			}else{
 				return true;
 			}
 		})
 	})
 	// Add the user ID in the enrollees array of the course
 	let isCourseUpdated = await Course.findById(data.courseId).then(course => {
 		// Adds the useId in the course's enrollees array
 		course.enrollees.push({userId:data.userId});
 		// Saves the updated course information in the database
 		return course.save().then((course, error) => {
 			if(error){
 				return false;
 			}else{
 				return true;
 			}
 		})
 	})
 	// Condition that will check if the user and course documents have been updated
 	if(isUserUpdated && isCourseUpdated){
 		return true;
 	}else{
 		return false;
 	}
 }
 */