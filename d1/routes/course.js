const express = require("express");
const router = express.Router();
const courseController = require("../controllers/course");
const auth = require("../auth")

// Route for creating a course
router.post("/",auth.verify, (req,res) => {
	const userData = auth.decode(req.headers.authorization)
	courseController.addCourse(req.body, {
		userId: userData.id, isAdmin:userData.isAdmin 
	})
	.then(resultFromController => 
		res.send(resultFromController))
})

// Answer
// router.post("/", auth.verify,(req,res)=>{
// const data = {
// 		course: req.body,
// 		isAdmin: auth.decode(req.headers.authorization).isAdmin
// 		}
// 		courseController.addCourse(data.then(resultFromController))
// })


// *Activity Instructions:*
// 1. Refactor the course route to implement user authentication for the admin when creating a course.

// 2. Refactor the addCourse controller method to implement admin authentication for creating a course.

// 3. Push to git with the commit message of Add activity code - S34.

// 4. Add the link in Boodle.

// *Steps in setting admin access for course creation*:
// 1. Create a conditional statement that will check if the user is an administrator.

// 2. Create a new Course object using the mongoose model and the information from the request body and the id from the header.

// 3. Save the new Course to the database

// router.post('/', (req, res) => {
// 	const userData = auth.decode(req.headers.authorization);
// 	userController.getProfile({userId:userData.id}).then(resultFromController => res.send(resultFromController));
// })

// Retrieve all courses
router.get("/all", (req, res) =>{
	courseController.getAllCourses().then(resultFromController => res.send(resultFromController));
})

// Retrieve all active courses
router.get("/", (req, res) => {
	courseController.getAllActive().then(resultFromController => res.send(resultFromController));
})

// Retrieve a specific course
router.get("/:courseId", (req, res) => {
	console.log(req.params.courseId);

	courseController.getCourse(req.params).then(resultFromController => res.send(resultFromController));
})
// Update a course
router.put("/:courseId", auth.verify, (req, res) => {
	courseController.updateCourse(req.params, req.body).then(resultFromController => res.send(resultFromController))
})

// 1. Create a route for archiving a course. The route must use JWT authentication and obtain the course ID from the url.
router.put("/:courseId/archive", auth.verify,(req,res) => {
	courseController.archiveCourse(req.param, req.body).then(result => res.send(result))
})



// *Activity instructions:*
// 1. Refactor the user route to implement user authentication for the enroll route.
// 2. Process a POST request at the /enroll route using postman to enroll a user to a course.
// 3. Push to git with the commit message of Add activity code - S36.
// 1. Refactor the user route to implement user authentication for the enroll route.
module.exports = router;