const express= require('express');
const router = express.Router();
const auth = require("../auth")

const userController = require('../controllers/user')
// Route for checking if the user's email already exists in the database
router.post('/checkEmail', (req,res)=>{
	userController.checkEmailExists(req.body).then(result => res.send(result))
})

// Route for User Registration
router.post('/register',(req, res)=>{
	userController.registerUser(req.body).then(result => res.send(result))
})

// Routes for authenticating a user
router.post('/login', (req,res) => {
	userController.loginUser(req.body).then(result => res.send(result))
})

// ******==>Activity<==******
// 1. Create a /details route that will accept the user’s Id to retrieve the details of a user.
// router.post('/details/:id', (req,res) =>{
// 	userController.getProfile(req.params.id).then(result => res.send(result))
// })

// router.post("/details", auth.verify, (req, res) => {
// 	// Provides the user's ID for the getProfile controller method
// 	userController.getProfile({userId:req.body.id}).then(resultFromController => res.send (resultFromController));
// })

// router.get("/details", auth.verify, (req, res) => {
// 	const userData =auth.decode(req.headers.authorization);

// 	// Provides the user's ID for the getProfile controller method
// 	userController.getProfile({userId : req.body.id}).then(resultFromController => res.send (resultFromController));
// })

//Routes for authenticating a user
router.post('/login', (req, res) => {
	userController.loginUser(req.body).then(result => res.send(result))
})

// Route for retrieving user details
router.get("/details",auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);
	// Provides the user's ID for the getProfile controller method
	userController.getProfile({userId : userData.id}).then(resultFromController => res.send(resultFromController));

});

// Route to enroll user to a course
// router.post("/enroll", (req,res) => {
//  	let data = {
//  		userId: req.body.userId,
//  		courseId: req.body.courseId
//  	}

//  	userController.enroll(data).then(resultFromController => res.send(resultFromController));
// })
/*
router.post("/enroll", (req, res) => {
	let data = {
		userId : req.body.userId,
		courseId : req.body.courseId
	}

	userController.enroll(data).then(resultFromController => res.send(resultFromController));
})
*/


// Route to enroll user to a course
router.post("/enroll", (req, res) => {
	let data = {
		userId : req.body.userId,
		courseId : req.body.courseId
	}

	userController.enroll(data).then(resultFromController => res.send(resultFromController));
})

// Route to enroll user to a course
router.post("/enroll", auth.verify, (req, res) => {

let data = {
userId : req.body.userId,
courseId : req.body.courseId
}

const userData = auth.decode(data)

userController.enroll(userData).then(resultFromController => res.send(resultFromController));
})

// 1. Refactor the user route to implement user authentication for the enroll route.
// 2. Process a POST request at the /enroll route using postman to enroll a user to a course.
// 3. Push to git with the commit message of Add activity code - S36.
// 4. Add the link in Boodle.

module.exports = router;
